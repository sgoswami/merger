Authors: Alexander Khanov & Soumyananda Goswami
with inputs from FTAG package maintainers, the CERN ATLAS Codebase, GitLab ATLAS Software Tutorials (https://atlassoftwaredocs.web.cern.ch/ABtutorial/), xAOD mini Derivation Tutorials (https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/XAODMiniTutorialDerivations) and members of the Algorithms group.

__What this package does:__ It combines the large radius tracking container with the regular ones. Only works for Release 22.

Clone this repo in your working directory as `git clone <clone method url>`

Follow these instructions to get cracking:
`cd` into the merger framework (after cloning), and then do:
```
source execute_build.sh
# To run the transformation, edit the file execute_xfer.sh and then do
source execute_xfer.sh
```
For each new shell/login, do: `each_login.sh` .
Setup rucio/VOMS using : `source grid_init.sh` .
For grid submissions, do: `cd run` & `source grid_submit.sh` .
For data query after setting up grid init, do: `source datasearch.sh` .
For locally running the transformation edit the file `execute_trf.sh` and do `source execute_trf.sh` .
