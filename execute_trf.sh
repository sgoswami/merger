#!/bin/bash
cd run
files="/eos/user/s/sgoswami/public/valid1.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.recon.AOD.e5362_s3227_r12627/*.pool.root.1"
for f in $files
do
  fbase=`basename $f`
  printf "Basename is $fbase \n"
  printf "Starting reduction on $f \n"
  Reco_tf.py --inputAODFile $f --outputDAODFile $fbase --reductionConf TEST1
  kill -9 $(ps aux | grep 'athena' | awk '{print $2}')
done
